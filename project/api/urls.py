# api/urls.py
from django.urls import path
from . views import login
from . views import register
from . views import ChangePasswordView
from . views import ChangeEmailView
from . views import emaillist
from . views import postid
from . views import activation
from . views import InKollektiv


urlpatterns = [


    path('user/login/', login),
    path('user/register/', register),
    path('user/ChangePasswordView/', ChangePasswordView),
    path('user/ChangeEmailView/', ChangeEmailView),
    path('user/EmailList/', emaillist),
    path('user/postid/', postid),
    path('user/activated/', activation),
    path('user/inKollektiv/', InKollektiv)

]
