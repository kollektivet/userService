from django.contrib.auth.models import User
from django import forms
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
