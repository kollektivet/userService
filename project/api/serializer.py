from django.contrib.auth.models import User
from rest_framework import serializers
from django.conf import settings

from django.contrib.auth.forms import PasswordResetForm


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password' : {'write_only' : True, 'required' : True }}

        def create(self, validated_data):
            user = User.objects.create(
                username=validated_data['username']
            )
            user.set_password(validated_data['password'])
            user.save()
            return user

class UsersSerializer(serializers.ModelSerializer):
    class Meta:

        model = User

        fields = ('username', 'password')


class ChangePasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

class ChangeEmailSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_email = serializers.CharField(required=True)
    new_email = serializers.CharField(required=True)

class ChangeNameSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_name = serializers.CharField(required=True)
    new_name = serializers.CharField(required=True)
