import hashlib
import uuid

import requests
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAdminUser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.utils import json
from rest_framework.views import APIView

from .serializer import ChangeEmailSerializer
from .serializer import ChangeNameSerializer
from .serializer import ChangePasswordSerializer

User = get_user_model()
from .models import User
domain = "http://localhost"

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    email = request.data.get("email")
    password = request.data.get("password")
    print(email)
    print(password)
    if email is None or password is None:
        return Response({'error': 'Please provide both email and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(email=email, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    if user.activated == False:
        return Response({'error': 'Aktiver brukeren din via email'},
                        status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key, 'id': token.user_id, 'name':user.name, 'email':token.email},
                    status=HTTP_200_OK)


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def register(request, ):
    email = request.data.get("email", "")
    password = request.data.get("password", "")

    salt = uuid.uuid4().hex
    activation_key = hashlib.sha512(email.encode('utf-8') + salt.encode('utf-8')).hexdigest()[:8]

    if not email and not password:
        return Response(
            data={
                "message": "username, password and email is required to register a user"
            },
            status=status.HTTP_400_BAD_REQUEST
        )
    new_user = User.objects.create_user(
        email=email, password=password, activated=False, confirmToken=activation_key
    )
    print(activation_key)
    url = domain+":8001/email/register/?activationcode=" + activation_key + "&recipient=" + email
    response = requests.post(url).text
    emailreg = json.loads(response)
    return Response(emailreg, status=status.HTTP_201_CREATED)


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
class ChangePasswordView(APIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (AllowAny,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response("Success.", status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(["POST"])
@permission_classes((IsAuthenticated,))
class ChangeEmailView(APIView):
    """
    An endpoint for changing email.
    """
    serializer_class = ChangeEmailSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old email
            if not self.object.check_email(serializer.data.get("old_email")):
                return Response({"old_email": ["Wrong email."]}, status=status.HTTP_400_BAD_REQUEST)
            self.object.set_email(serializer.data.get("new_email"))
            self.object.save()
            return Response("Success.", status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(["POST"])
@permission_classes((IsAuthenticated,))
class ChangeNameView(APIView):
    """
    An endpoint for changing first name.
    """
    serializer_class = ChangeNameSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old email
            if not self.object.check_email(serializer.data.get("old_name")):
                return Response({"old_name": ["Wrong name."]}, status=status.HTTP_400_BAD_REQUEST)
            self.object.set_first_name(serializer.data.get("new_name"))
            self.object.save()
            return Response("Success.", status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(["GET"])
@permission_classes((IsAdminUser,))
def emaillist(request, ):
    emails = User.objects.values_list('email', flat=True)
    emailList = []
    emailList.append(emails)
    return Response(emailList)


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def postid(request):
    token = request.data.get('token')
    token2 = Token.objects.get(key=token)
    uid = token2.user_id
    userid = str(uid)
    url = "https://kollektivet.app:8083/kollektiv/getUserTasks/?id=" + userid
    response = requests.post(url).text
    t2 = json.loads(response)
    return Response(t2)


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def isActive(request, ):
    activated = request.data.get("activate")
    if (activated == 1):
        return Response(activated)


@csrf_exempt
@api_view(["GET"])
@permission_classes((AllowAny,))
def activation(request, ):
    aktiveringskode = request.GET.get("aktiveringskode", "")
    print(aktiveringskode)
    profile = get_object_or_404(User, confirmToken=aktiveringskode)
    print(profile)
    print(profile.activated)

    if profile.activated == False:
        profile.activated = True
        profile.save()
    return HttpResponseRedirect(domain+":3000")


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def InKollektiv(request):
    token = request.POST.get("token")
    token2 = Token.objects.get(key=token)
    uid = token2.user_id
    userid = str(uid)
    print(userid)
    url = domain+":8083/kollektiv/isMember/?userid=" + userid
    response = requests.post(url).text
    t2 = json.loads(response)
    return Response(t2)
