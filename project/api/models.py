from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def set_password(self, password):
        user = self.model(password=password)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def set_activated(self,email):
        user = self.model(email=email)
        user.set_activated(email)
        user.self.model(activated=True)
        user.save(using=self._db)
        return user

    def check_email(self, email, password):
        user = self.model(email=email)
        user.set_password(password)
        return user

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """User model."""

    username = None
    last_name = None
    first_name = None
    email = models.EmailField(_('email address'), unique=True)
    name = models.CharField(max_length=100)
    activated = models.BooleanField(default=False)
    confirmToken = models.CharField(max_length=80, default='')
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()